/***
 *
 * Course Module
 *
 *
 */

 import getters from "./getters";
 import mutations from "./mutations";
 import actions from "./actions";
 
 export default {
   state: {
     course: {
       name: "",
       category: "",
       slug: "",
       description: "",
       currency: "",
       code: "",
       price: "",
       cover: "",
       video: "",
       cstate:"",
       about:""
     }
   },
   getters,
   actions,
   mutations
 };