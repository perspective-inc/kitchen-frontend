export default {
    SET_COURSE_DETAILS(state, data) {
      console.log(data, "data");
      const { name, category,cstate, slug, currency, price, description, code,about} = data;
      state.course.name = name;
      state.course.category = category;
      state.course.slug = slug;
      state.course.currency = currency;
      state.course.price = price;
      state.course.about = about;
      state.course.code = code;
      state.course.description = description;
      state.course.cstate = cstate;
     
    },
    SET_MEDIA_DETAILS(state, data) {
      console.log(data, "data");
      const { cover,video}=data;
        state.course.video = video;
        state.course.cover = cover;
      }
}