import Vuex from "vuex";
import course from "./modules/course";

const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      course
    },
    state: {
     
    }
  });
};
export default createStore;